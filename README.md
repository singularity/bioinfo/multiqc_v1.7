# Package multiqc Version 1.14
https://github.com/ewels/MultiQC

https://multiqc.info/


MultiQC is a tool to create a single report with interactive plots for multiple bioinformatics analyses across many samples.


Package installation using Miniconda3 V4.7.12 All packages are in /opt/miniconda/bin & are in PATH multiqc Version: 1.7
Singularity container based on the recipe: Singularity.multiqc_v1.14
## Local build:
```bash
sudo singularity build multiqc_v1.14.sif Singularity.multiqc_v1.14
```
## Get image help:

```bash
singularity run-help multiqc_v1.14.sif
```
### Default runscript: multiqc
## Usage:
```bash
./multiqc_v1.14.sif --help
```
or:
```bash
singularity exec multiqc_v1.14.sif multiqc --help
```
image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:


```bash
singularity pull multiqc_v1.14.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/multiqc_v1.14/multiqc_v1.14:latest
```
